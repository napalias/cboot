<?php

namespace Cboot;

use Illuminate\Support\ServiceProvider;

class CbootProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/bootstrap', 'cb');
    }
}
