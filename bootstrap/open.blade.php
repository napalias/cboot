<form action="{{$action}}" method="{{$method}}">
    {{csrf_field()}}
    {{method_field($method_field)}}