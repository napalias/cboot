<?php
$name = isset($name) ? $name : str_slug(strtolower($label));
$for = isset($for) ? $for : $name;
$class = isset($class) ? $class : $name;
$id = isset($id) ? $id : $for;
$value = old($name, isset($value) ? (is_object($value) ? $value->$name : $value) : '');

?>

<div class="form-group">
    <label for="{{$for}}">Language</label>
    <select name="{{$name}}" id="{{$id}}" class="form-control {{$class}}">
        @foreach($options as $key =>$option)
            <option value="{{$key}}" @if($key == $value && !is_null($value)) selected @endif>{{$option}}</option>
        @endforeach
    </select>
</div>
