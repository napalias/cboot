<form action="{{route($route, $model)}}" method="post">
    {{csrf_field()}}
    {{method_field('put')}}
    {{$slot}}
</form>