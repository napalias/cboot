<form action="{{route($route)}}" method="post">
    {{csrf_field()}}
    {{$slot}}
</form>