<?php
	$name = isset($name) ? $name : str_slug(strtolower($label));
	$for = isset($for) ? $for : $name;
	$class = isset($class) ? $class : $name;
	$id = isset($id) ? $id : $for;


	$value = old($name, isset($value) ? (is_object($value) ? $value->$name : $value) : '');
	$placeholder = isset($placeholder) ? $placeholder : $label;
    $rows = isset($rows) ? $rows : 3;

    $extra = $extra ?? '';
?>

<div class="form-group">
    @if(isset($label))
        <label for="{{$name}}">{{$label}}</label>
    @endif
    <textarea
            class="form-control {{$class}}"
            name="{{$name}}"
            id="{{$id}}"
            placeholder="{{$placeholder}}"
            rows="{{$rows}}"
            {!!$extra!!}
    >{{$value}}</textarea>

    @if ($errors->has($name))
        <span class="help-block">! {{ $errors->first($name) }}</span>
    @endif
</div>