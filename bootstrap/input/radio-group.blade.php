<?php
$for = $for ?? $name;
$class = $class ??  : $name;
$id = $id ?? $for;
$selected = old($name, isset($selected) ? $selected : null);
$options = isset($options) && is_array($options) ? $options : [];
?>
<div class="form-group @if($errors->has($name)) has-error @endif">
    @foreach($options as $key=>$option)
        @include('form.input.radio', ['name' => $name, 'label'=>$option, 'value'=>$key])
    @endforeach

    @if ($errors->has($name))
        <span class="help-block">! {{ $errors->first($name) }}</span>
    @endif
</div>