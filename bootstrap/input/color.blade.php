@php
    $name = isset($name) ? $name : str_slug(strtolower($label));
    $for = isset($for) ? $for : $name;
    $class = isset($class) ? $class : $name;
    $id = isset($id) ? $id : $for;
    $value = old($name, isset($value) ? (is_object($value) ? $value->$name : $value) : '');
    $placeholder = isset($placeholder) ? $placeholder : $label;
    $type = 'color';
    $extra = $extra ?? '';
@endphp
<div class="form-group @if($errors->has($name)) has-error @endif">
    @if(isset($label))
        <label for="{{$name}}">{{$label}}</label>
    @endif
    <div class="form-control">
    <input class="{{$class}}"
           type="{{$type}}"
           name="{{$name}}"
           id="{{$id}}"
           placeholder="{{$placeholder}}"
           value="{{$value}}"
           @if(!empty($required)) required @endif
           {{$extra}}
    >
    </div>

    @if ($errors->has($name))
        <span class="help-block">! {{ $errors->first($name) }}</span>
    @endif
</div>
