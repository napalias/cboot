<p class="text-center">
    <button type="submit" class="btn {{ $class ?? 'btn-primary'}}">{{$slot ?? 'Submit'}}</button>
</p>